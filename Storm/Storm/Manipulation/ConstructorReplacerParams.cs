﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storm.Manipulation
{
    public struct ConstructorReplacerParams
    {
        public string FromClass { get; set; }
        public string ToClass { get; set; }
    }
}
